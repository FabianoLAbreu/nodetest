var express = require ("express"),
    load = require ("express-load"),
    http = require ("http"),
    bodyParser = require ("body-parser"),
    methodOverride = require('method-override'),
    app = express()
;

app.set("views", __dirname + "views");
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.set('port', (process.env.PORT || 3000));

load("models")
    .then("controllers")
    .then("routes")
    .into(app);

var server = http.createServer(app).listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});