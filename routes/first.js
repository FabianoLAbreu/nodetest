module.exports = function(app){
    var first = app.controllers.first;
    app.get("/first/list", first.list);
    app.get("/first/listbyid/:id", first.listById);
    app.post("/first/test", first.test);
};